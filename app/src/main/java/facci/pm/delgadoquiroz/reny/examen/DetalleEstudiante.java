package facci.pm.delgadoquiroz.reny.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import facci.pm.delgadoquiroz.reny.examen.database.entidad.ItemDB;
import facci.pm.delgadoquiroz.reny.examen.database.modelo.Estudiante;
import facci.pm.delgadoquiroz.reny.examen.database.modelo.Materia;

public class DetalleEstudiante extends AppCompatActivity {

    ItemDB itemDB = new ItemDB(this);
    TextView cedula;
    TextView nombre;
    TextView apellido;
    TextView nivel;
    TextView materiasAprobadas;
    TextView materiasReprobadas;
    ListView listViewMaterias;

    ArrayList<Materia> listaDeMaterias = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_estudiante);

        listViewMaterias = (ListView) findViewById(R.id.lvListaMaterias);

        cedula = (TextView) findViewById(R.id.lvCedula);
        nombre = (TextView) findViewById(R.id.lvNombre);
        apellido = (TextView) findViewById(R.id.lvApellido);
        nivel = (TextView) findViewById(R.id.lvNivel);
        materiasAprobadas = (TextView) findViewById(R.id.lvMateriasAprobadas);
        materiasReprobadas = (TextView) findViewById(R.id.lvMateriasReprobadas);

        String cedu = getIntent().getStringExtra("cedula");

        Estudiante estudiante = itemDB.consultaIndividualEstudiante(cedu);


        listaDeMaterias.addAll(itemDB.obtenerMateriasEstudiantes(cedu));

        final ArrayAdapter<Materia> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaDeMaterias);
        listViewMaterias.setAdapter(adapter);

        //Materia materia = itemDB.consultaIndividualMateria(cedu);

        cedula.setText(estudiante.getCedulaEstudiante());
        nombre.setText(estudiante.getNombre());
        apellido.setText(estudiante.getApellido());
        nivel.setText(estudiante.getNivel());
        materiasAprobadas.setText(estudiante.getMateriasAprobadas());
        materiasReprobadas.setText(estudiante.getMateriasReprobadas());



    }
}
