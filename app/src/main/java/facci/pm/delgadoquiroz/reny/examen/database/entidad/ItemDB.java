package facci.pm.delgadoquiroz.reny.examen.database.entidad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

import facci.pm.delgadoquiroz.reny.examen.database.helper.ClaseHelper;
import facci.pm.delgadoquiroz.reny.examen.database.modelo.Estudiante;
import facci.pm.delgadoquiroz.reny.examen.database.modelo.Materia;

public class ItemDB {

    ClaseHelper claseHelper;

    public ItemDB(Context context) {
        claseHelper = new ClaseHelper(context);
    }

    /* Clase interna que define el contenido de cada tabla  */
    public static abstract class ElementosDeEntrada implements BaseColumns {

        //TABLA Estudiante
        public static final String TABLE_NAME_ESTUDIANTE = "Estudiante";
        public static final String COLUMN_NAME_CedulaEstudiante = "cedulaEstudiante";
        public static final String COLUMN_NAME_Nombre = "nombre";
        public static final String COLUMN_NAME_Apellido = "apellido";
        public static final String COLUMN_NAME_Nivel = "nivel";
        public static final String COLUMN_NAME_MateriasAprobadas = "materiasAprobadas";
        public static final String COLUMN_NAME_MateriasReprobadas = "materiasReprobadas";

        public static final String CREATE_TABLE_ESTUDIANTE = "CREATE TABLE " +
                TABLE_NAME_ESTUDIANTE + " (" +
                "id" + " INTEGER PRIMARY KEY, "  +
                COLUMN_NAME_CedulaEstudiante + " TEXT, "  +
                COLUMN_NAME_Nombre + " TEXT, "  +
                COLUMN_NAME_Apellido + " TEXT, " +
                COLUMN_NAME_Nivel + " TEXT, "  +
                COLUMN_NAME_MateriasAprobadas + " TEXT, "  +
                COLUMN_NAME_MateriasReprobadas + " TEXT  )";
        public static final String DELETE_TABLE_ESTUDIANTE = "DROP TABLE IF EXISTS " + TABLE_NAME_ESTUDIANTE;

        //TABLA MATERIAS
        public static final String TABLE_NAME_MATERIAS = "Materias";
        public static final String COLUMN_NAME_CedulaEstudianteM = "cedulaEstudiante";
        public static final String COLUMN_NAME_NombreMateria = "nombreMateria";
        public static final String COLUMN_NAME_Nota1 = "nota1";
        public static final String COLUMN_NAME_Nota2 = "nota2";
        public static final String COLUMN_NAME_Estado = "estado";

        public static final String CREATE_TABLE_MATERIAS = "CREATE TABLE " +
                TABLE_NAME_MATERIAS + " (" +
                "id" + " INTEGER PRIMARY KEY, "  +
                COLUMN_NAME_CedulaEstudianteM + " TEXT, " +
                COLUMN_NAME_NombreMateria + " TEXT, " +
                COLUMN_NAME_Nota1 + " TEXT, " +
                COLUMN_NAME_Nota2 + " TEXT, " +
                COLUMN_NAME_Estado+ " TEXT)";
        public static final String DELETE_TABLE_MATERIAS = "DROP TABLE IF EXISTS " + TABLE_NAME_MATERIAS;

    }


    int c = 0;
    public void insertarEstudiante(String cedulaEstudiante, String nombre, String apellido, String nivel, String materiasAprobadas, String materiasReprobadas){
        SQLiteDatabase base = claseHelper.getWritableDatabase();
        ContentValues contenido = new ContentValues();

        contenido.put("cedulaEstudiante", cedulaEstudiante);
        contenido.put("nombre", nombre);
        contenido.put("apellido", apellido);
        contenido.put("nivel", nivel);
        contenido.put("materiasAprobadas", materiasAprobadas);
        contenido.put("materiasReprobadas", materiasReprobadas);

        base.insert(ElementosDeEntrada.TABLE_NAME_ESTUDIANTE, null, contenido);
        claseHelper.getWritableDatabase().close();
        c=c+1;
        Log.e("Insartando en BASEDATOS", "INSERTADO CORRECTAMENTE "+c);
    }



    int d=0;
    public void insertarMaterias(String cedulaEstudianteM, String nombreMateria, String nota1, String nota2, String estadoMateria){
        SQLiteDatabase base = claseHelper.getWritableDatabase();
        ContentValues contenidoMateria = new ContentValues();
        contenidoMateria.put(ElementosDeEntrada.COLUMN_NAME_CedulaEstudianteM, cedulaEstudianteM);
        contenidoMateria.put(ElementosDeEntrada.COLUMN_NAME_NombreMateria, nombreMateria);
        contenidoMateria.put(ElementosDeEntrada.COLUMN_NAME_Nota1, nota1);
        contenidoMateria.put(ElementosDeEntrada.COLUMN_NAME_Nota2, nota2);
        contenidoMateria.put(ElementosDeEntrada.COLUMN_NAME_Estado, estadoMateria);

        base.insert(ElementosDeEntrada.TABLE_NAME_MATERIAS, null, contenidoMateria);
        d=d+1;
        claseHelper.getWritableDatabase().close();
        Log.e("Insartando Materias", "INSERTADO CORRECTAMENTE "+d);
    }





    public Materia consultaIndividualMateria(String cedula){
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        Materia materia = null;

        Cursor fila = base.rawQuery("Select * From Materia where cedulaEstudiante = " + cedula, null);
        if(fila.moveToFirst()){
            String id = fila.getString(fila.getColumnIndex("id"));
            String cedulaEstudianteM = fila.getString(fila.getColumnIndex("cedulaEstudiante"));
            String nombreMateria = fila.getString(fila.getColumnIndex("nombreMateria"));
            String nota1 = fila.getString(fila.getColumnIndex("nota1"));
            String nota2 = fila.getString(fila.getColumnIndex("nota2"));
            String estadoMateria = fila.getString(fila.getColumnIndex("estado"));

            materia = new Materia(Integer.parseInt(id), cedulaEstudianteM,nombreMateria,nota1,nota2,estadoMateria);

        }
        else{
            Log.e("NO DATO: ", "Registro no Encontrado");
            return null;
        }
        claseHelper.getReadableDatabase().close();
        base.close();
        return materia;

    }


    public ArrayList<Materia> obtenerMateriasEstudiantes(String cedula){
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        ArrayList<Materia> listaMaterias = new ArrayList<>();

        Cursor fila = base.rawQuery("SELECT * FROM Materias where cedulaEstudiante = " + cedula, null);
        if(fila.moveToFirst()){

            do {
                String id = fila.getString(fila.getColumnIndex("id"));
                String ceduEstudiante = fila.getString(fila.getColumnIndex("cedulaEstudiante"));
                String nombreMateria = fila.getString(fila.getColumnIndex("nombreMateria"));
                String nota1Estud = fila.getString(fila.getColumnIndex("nota1"));
                String nota2Estud = fila.getString(fila.getColumnIndex("nota2"));
                String estadoMateria = fila.getString(fila.getColumnIndex("estado"));


                Materia materia = new Materia(Integer.parseInt(id), ceduEstudiante, nombreMateria, nota1Estud, nota2Estud, estadoMateria);
                listaMaterias.add(materia);

            }while (fila.moveToNext());
            fila.close();
        }
        else {
            Log.e("NO datos Cursor", "NO hay datos");
        }
        base.close();
        return listaMaterias;
    }







    public Estudiante consultaIndividualEstudiante(String cedula){
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        Estudiante estudiante = null;

        Cursor fila = base.rawQuery("Select * From Estudiante where cedulaEstudiante = " + cedula, null);
        if(fila.moveToFirst()){
            String id = fila.getString(fila.getColumnIndex("id"));
            String cedulaEstudiante = fila.getString(fila.getColumnIndex("cedulaEstudiante"));
            String nombre = fila.getString(fila.getColumnIndex("nombre"));
            String apellido = fila.getString(fila.getColumnIndex("apellido"));
            String nivel = fila.getString(fila.getColumnIndex("nivel"));
            String materiasAprobadas = fila.getString(fila.getColumnIndex("materiasAprobadas"));
            String materiasReprobadas = fila.getString(fila.getColumnIndex("materiasReprobadas"));

            estudiante = new Estudiante(Integer.parseInt(id), cedulaEstudiante, nombre, apellido, nivel, materiasAprobadas, materiasReprobadas);

        }
        else{
            Log.e("NO DATO: ", "Registro no Encontrado");
            return null;
        }
        claseHelper.getReadableDatabase().close();
        base.close();
        return estudiante;

    }
    public ArrayList<Estudiante> obtenerEstudiantes(){
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        ArrayList<Estudiante> listaPasajes = new ArrayList<>();

        Cursor fila = base.rawQuery("SELECT * FROM "+ElementosDeEntrada.TABLE_NAME_ESTUDIANTE, null);
        if(fila.moveToFirst()){

            do {
                String id = fila.getString(fila.getColumnIndex("id"));
                String ceduEstudiate = fila.getString(fila.getColumnIndex("cedulaEstudiante"));
                String nombreEstud = fila.getString(fila.getColumnIndex("nombre"));
                String apellidoEstud = fila.getString(fila.getColumnIndex("apellido"));
                String nivelEstud = fila.getString(fila.getColumnIndex("nivel"));
                String materiasApro = fila.getString(fila.getColumnIndex("materiasAprobadas"));
                String materiasRepro = fila.getString(fila.getColumnIndex("materiasReprobadas"));

                Estudiante pasaje = new Estudiante(Integer.parseInt(id), ceduEstudiate, nombreEstud, apellidoEstud, nivelEstud, materiasApro, materiasRepro);
                listaPasajes.add(pasaje);

            }while (fila.moveToNext());
            fila.close();
        }
        else {
            Log.e("NO datos Cursor", "NO hay datos");
        }
        base.close();
        return listaPasajes;
    }






    public void eliminarBases(){

        SQLiteDatabase base = claseHelper.getWritableDatabase();

        base.delete(ElementosDeEntrada.TABLE_NAME_ESTUDIANTE, null, null);
        base.delete(ElementosDeEntrada.TABLE_NAME_MATERIAS, null, null);
        base.close();
    }

}
