package facci.pm.delgadoquiroz.reny.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import facci.pm.delgadoquiroz.reny.examen.database.entidad.ItemDB;
import facci.pm.delgadoquiroz.reny.examen.database.modelo.Estudiante;

public class MainActivity extends AppCompatActivity {

    ItemDB itemDB = new ItemDB(this);
    EditText editTextCedula;
    Button btnConsultarCedula;
    Button btnListar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextCedula = (EditText) findViewById(R.id.editTextCedula);
        btnConsultarCedula = (Button) findViewById(R.id.btnConsultaEstudiante);
        btnListar= (Button) findViewById(R.id.btnListar);



        //Iniciamos Eliminando todas las Bases de Datos...      Esto es para que no hayan registros duplicados.
        itemDB.eliminarBases();

        insertarEstudianteMain();
        insertarMateriasMain();
        String rutaBaseEXAMEN = getDatabasePath("EXAMEN.db").getAbsolutePath();
        Log.e("Ruta", rutaBaseEXAMEN);


        btnConsultarCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valor = editTextCedula.getText().toString();

                if(!valor.isEmpty()){
                    consultaCedula(valor);
                    editTextCedula.setText("");
                }
                else {
                    Toast.makeText(getApplicationContext(), "Ingrese un valor!!!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListaEstudiantes.class);
                startActivity(i);
            }
        });

    }

    private void insertarMateriasMain() {
        itemDB.insertarMaterias("1314023993", "Sistemas Expertos", "10", "10", "Aprobado");
        itemDB.insertarMaterias("1234567890", "Programación Móvil", "8", "7", "Aprobado");
        itemDB.insertarMaterias("1234567890", "Proyecto Integrador", "6", "8", "Aprobado");
        itemDB.insertarMaterias("2222222222", "Programación Móvil", "5", "5", "Reprobado");
        itemDB.insertarMaterias("5555555555", "Base de Datos", "4", "6", "Reprobado");
        itemDB.insertarMaterias("2222222222", "Ecuaciones", "5", "3", "Reprobado");
        itemDB.insertarMaterias("2222222222", "Programación Móvil", "5", "9", "Aprobado");
        itemDB.insertarMaterias("5555555555", "Sistemas Expertos", "4", "6", "Reprobado");
    }

    private void insertarEstudianteMain() {
        Log.e("MainInsertar", "Insertando Pasaje desde Main");
        itemDB.insertarEstudiante("1314023993", "Gabriel", "Arias", "4", "2", "4");
        itemDB.insertarEstudiante("1234567890", "Reny", "Delgado", "5", "4", "2");
        itemDB.insertarEstudiante("2222222222", "Marcos", "Loor", "4", "6", "1");
        itemDB.insertarEstudiante("5555555555", "Laura", "Lopez", "4", "8", "0");


    }



    private void consultaCedula(String cedula){
        Estudiante estudiante = itemDB.consultaIndividualEstudiante(cedula);
        Intent i = new Intent(MainActivity.this, DetalleEstudiante.class);
        i.putExtra("cedula", estudiante.getCedulaEstudiante());
        i.putExtra("nombre", estudiante.getNombre());
        i.putExtra("apellido", estudiante.getApellido());
        i.putExtra("nivel", estudiante.getNivel());
        i.putExtra("mAprobada", estudiante.getMateriasAprobadas());
        i.putExtra("mReprobada", estudiante.getMateriasReprobadas());
        startActivity(i);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        itemDB.eliminarBases();
        Toast.makeText(this, "Aplicación Destruida",Toast.LENGTH_LONG).show();
        Log.e("Ciclo", "OnDestroy");
    }


}
