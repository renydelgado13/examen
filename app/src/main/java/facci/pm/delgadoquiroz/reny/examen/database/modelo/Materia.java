package facci.pm.delgadoquiroz.reny.examen.database.modelo;

import androidx.annotation.NonNull;

public class Materia {
    private int parseInt;
    private String cedulaEstudiante;
    private String nombreMateria;
    private String nota1;
    private String nota2;
    private String estadoMateria;

    public Materia(int parseInt, String cedulaEstudiante, String nombreMateria, String nota1, String nota2, String estadoMateria) {
        this.parseInt = parseInt;
        this.cedulaEstudiante = cedulaEstudiante;
        this.nombreMateria = nombreMateria;
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.estadoMateria = estadoMateria;

    }

    public int getParseInt() {
        return parseInt;
    }

    public void setParseInt(int parseInt) {
        this.parseInt = parseInt;
    }

    public String getCedulaEstudiante() {
        return cedulaEstudiante;
    }

    public void setCedulaEstudiante(String cedulaEstudiante) {
        this.cedulaEstudiante = cedulaEstudiante;
    }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    public String getNota1() {
        return nota1;
    }

    public void setNota1(String nota1) {
        this.nota1 = nota1;
    }

    public String getNota2() {
        return nota2;
    }

    public void setNota2(String nota2) {
        this.nota2 = nota2;
    }

    public String getEstadoMateria() {
        return estadoMateria;
    }

    public void setEstadoMateria(String estadoMateria) {
        this.estadoMateria = estadoMateria;
    }

    @NonNull
    @Override
    public String toString() {
        return parseInt+"   "+nombreMateria+"   "+nota1+"   "+nota2+"   "+estadoMateria;
    }

}
