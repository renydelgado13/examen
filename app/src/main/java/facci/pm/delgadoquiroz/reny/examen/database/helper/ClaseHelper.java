package facci.pm.delgadoquiroz.reny.examen.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import facci.pm.delgadoquiroz.reny.examen.database.entidad.ItemDB;

public class ClaseHelper extends SQLiteOpenHelper {


    public static int versionDB = 1;
    public static String nombreDB= "EXAMEN.db" ;

    public ClaseHelper(@Nullable Context context) {
        super(context, nombreDB, null, versionDB);
    }

    @Override
    public void onCreate(SQLiteDatabase baseDeDatos) {

        baseDeDatos.execSQL(ItemDB.ElementosDeEntrada.CREATE_TABLE_ESTUDIANTE);
        baseDeDatos.execSQL(ItemDB.ElementosDeEntrada.CREATE_TABLE_MATERIAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase baseDeDatos, int oldVersion, int newVersion) {

    }
}
