package facci.pm.delgadoquiroz.reny.examen.database.modelo;

public class Estudiante {

    private int parseInt;
    private String cedulaEstudiante;
    private String nombre;
    private String apellido;
    private String nivel;
    private String materiasAprobadas;
    private String materiasReprobadas;

    public Estudiante(int parseInt, String cedulaEstudiante, String nombre, String apellido, String nivel, String materiasAprobadas, String materiasReprobadas) {

        this.parseInt = parseInt;
        this.cedulaEstudiante = cedulaEstudiante;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nivel = nivel;
        this.materiasAprobadas = materiasAprobadas;
        this.materiasReprobadas = materiasReprobadas;
    }

    public int getParseInt() {
        return parseInt;
    }

    public void setParseInt(int parseInt) {
        this.parseInt = parseInt;
    }

    public String getCedulaEstudiante() {
        return cedulaEstudiante;
    }

    public void setCedulaEstudiante(String cedulaEstudiante) {
        this.cedulaEstudiante = cedulaEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getMateriasAprobadas() {
        return materiasAprobadas;
    }

    public void setMateriasAprobadas(String materiasAprobadas) {
        this.materiasAprobadas = materiasAprobadas;
    }

    public String getMateriasReprobadas() {
        return materiasReprobadas;
    }

    public void setMateriasReprobadas(String materiasReprobadas) {
        this.materiasReprobadas = materiasReprobadas;
    }


    @Override
    public String toString() {
        return parseInt+"   "+cedulaEstudiante+"   "+nombre+"   "+apellido+"   "+nivel+"   "+materiasAprobadas+"   "+materiasReprobadas;
    }
}
