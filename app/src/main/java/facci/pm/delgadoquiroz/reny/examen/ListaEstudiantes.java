package facci.pm.delgadoquiroz.reny.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import facci.pm.delgadoquiroz.reny.examen.database.entidad.ItemDB;
import facci.pm.delgadoquiroz.reny.examen.database.modelo.Estudiante;

public class ListaEstudiantes extends AppCompatActivity {

    ListView listViewEstudiantes;
    ItemDB itemDB = new ItemDB(this);
    ArrayList<Estudiante> listaDeEstudiantes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_estudiantes);

        listViewEstudiantes = (ListView) findViewById(R.id.listView1);

        listaDeEstudiantes.addAll(itemDB.obtenerEstudiantes());


        final ArrayAdapter<Estudiante> adapter = new ArrayAdapter<>(this, R.layout.list_view_disenado, listaDeEstudiantes);
        listViewEstudiantes.setAdapter(adapter);

    }
}
